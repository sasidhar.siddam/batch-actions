import boto3
import base64
from botocore.exceptions import ClientError
import json
import psycopg2
import sys
import os
import io

def job_exec(sql_id, secret_name, service_name, sql_params, region_name, result_file_name, bucket_name, access_key, secret_key):

    s3 = boto3.resource('s3',aws_access_key_id=access_key,aws_secret_access_key=secret_key)

    s3_object = s3.Object(bucket_name, sql_id)
    sql = s3_object.get()['Body'].read().decode('utf-8')

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name=service_name,
        region_name=region_name
    )
    
    response = client.get_secret_value(
    SecretId=secret_name
    )

    secret = json.loads(response['SecretString'])
    
    db_host=secret['host']
    db_user=secret['username']
    db_pwd=secret['password']
    db_name=secret['dbname']

    json_object = json.loads(sql_params)

    for key in json_object:
        sql = sql.replace(':'+key,"'"+json_object[key]+"'")

    conn = psycopg2.connect(database=db_name, user=db_user,password=db_pwd, host=db_host)
    cur = conn.cursor()

    cur.execute(sql)
    results = cur.fetchall()
    bodyResult = str(results[0])
    
    s3_object = s3.Object(bucket_name, result_file_name)
    s3_object.put(Body=bodyResult)    
    cur.close()
    conn.close() 

#print(sys.argv[1:])
job_exec(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7], sys.argv[8], sys.argv[9])
